Some of the most powerful tools at our disposal are our internet connected smart phones.
Instant communication and the sum of all human knowledge at our fingertips massively increases our ability to affect the world around us.
But this connectivity comes at the cost of increased surveillance by State security apparatuses and private individuals.
Those who are active in liberatory movements are aware---to varying degrees---of this surveillance, and collectively we have developed operational security (OpSec) practices and an internal security culture to counter disruptions to our efforts to organize.

There are many urban legends around phones use that stem from inaccurate understandings of the technology used by phones and what capabilities the State and private actors have at their disposal to surveil individuals via their phones.
Threat modeling is the process of identifying threats and building specific and pragmatic countermeasures against them, but without accurate models of one's adversaries, such models lead to ineffective countermeasures.
Action taken based off misinformation can lead to easy arrest or create the impression of an all-seeing adversary thus stifling action.
This zine covers the basic technologies of phones and addresses common urban legends so that you and your comrades can resist disruption and organize effectively.

There is no such thing as perfect security.
It is not a binary that is either "on" or "off," nor is it even a spectrum of "better security" or "worse security."
Security is best discussed as "more secure under these conditions against those threats."
What might be effective for keeping the State from tracking your location via your phone might be useless for keeping an abusive partner from reading your messages.
This guide will help you understand the risks you face so you can make informed decisions.
Security culture is not a guarantee of safety, but it is harm reduction.
It may prevent your imprisonment or save your life or the lives of those around you.

This zine was written at the start of 2022 by anarchists in Europe and North America, and as such this knowledge will be most relevant to those close to us in both space and time.
We intentionally omit (most) legal considerations; just because your adversary isn't allowed to do something doesn't mean they won't do it anyway.
We instead focus on what is technically possible.
We also acknowledge the biases we can't see past (they can still be found in this zine), and we are not able to predict the future.
You will have to use the knowledge of your personal and local contexts to adapt what is written here to the specific threats you face.
